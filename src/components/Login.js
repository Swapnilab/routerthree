import React from 'react'
import { Navigate, useNavigate } from 'react-router-dom';

const Login = ({loginuser , checkuser}) => {
const navigate=useNavigate();
console.log(checkuser);
let login=()=>{
        loginuser();
        navigate("/dashboard");
 };
 
 if(checkuser){
  return (<Navigate to="/dashboard"/>)
 }
    return (
    <div>
       <h3> You have to login first to see the content..</h3>
        <button className='btn btn-primary' onClick={login}>Login</button>
    </div>
  )
}

export default Login