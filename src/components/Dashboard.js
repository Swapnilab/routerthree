import React from 'react'
import { Navigate, useNavigate } from 'react-router-dom';

const Dashboard = ({logoutuser , checkuser}) => {
    const navigate=useNavigate();
    let logout=()=>{
        logoutuser();
        navigate("/");

    }
    if(!checkuser)
    {
      return (<Navigate to="/login"/>)
    }
  return (
    <div><h3>User : Username you have logged in </h3>
    <button className='btn btn-danger' onClick={logout}>Logout</button></div>
  )
}

export default Dashboard