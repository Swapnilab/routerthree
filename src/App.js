import React, { Suspense } from 'react';
import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import Home from "./components/Home";
import Login from "./components/Login";
import Navbar from "./components/Navbar";
import NotFound from "./components/NotFound";
//import Register from "./components/Register";
const LazyRegister= React.lazy(()=>import("./components/Register"));
const LazyHome= React.lazy(()=>import("./components/Home"));
const LazyLogin= React.lazy(()=>import("./components/Login"));
const LazyDashboard= React.lazy(()=>import("./components/Dashboard"));
const LazyNotFound= React.lazy(()=>import("./components/NotFound"));
const LazyNavbar= React.lazy(()=>import("./components/Navbar"));

function App() {
  let [user, setUser] = useState(false);
  console.log("in app on app load_state", user);
  useEffect( ()=>{ 

    const user1=localStorage.getItem('login');
    console.log("in useEffect_start_local" , user1);
    user1?setUser(true):setUser(false);
    console.log("in useEffect_start state" , user);
  } , [])
  
  useEffect(() => {
    localStorage.setItem('login', user);
  }, [user]
  )



  return (
    <>
      <Suspense fallback={<h2>loading....</h2>}>
      <LazyNavbar checkuser={user} />
      
      <Routes>

        <Route path="login" element={<LazyLogin checkuser={user} loginuser={() => setUser(true)} />} />
         <Route path="dashboard" element={<LazyDashboard logoutuser={() => setUser(false)} checkuser={user} />} />
        <Route path="/" element={<LazyHome />} />

        <Route path="register" element={<LazyRegister />} />

        <Route path="*" element={<LazyNotFound />} />
      </Routes>
      </Suspense>
    </>
  );
}

export default App;
